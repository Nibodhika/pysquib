import sys

from pysquib.game import Game


if __name__ == "__main__":
    if len(sys.argv) < 2:
        print("You need to provide the path to a yaml file")

    file = sys.argv[1]
    print("Working with ", file)
    game = Game.load(file)

    game.save()
    # for card in game.cards:
    #     card.show()
