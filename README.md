# PySquib

Pysquib is a python deck prototyping tool inspired by squib (http://squib.rocks/).

The main difference is that pysquib aims to provide a way of generating decks without having to write code.
Instead relying on a ymal/json file to define layouts and other yaml/json/csv/xlsx files to define contents

# Example of use

Let's begin by writing our first game file and save it as `game.yaml`:

```yaml
decks:
  weapon:
    elements:
        name:
          type: text
    data:
      - name: Sword
      - name: Axe
```

With that we told pysquib that we want to create a deck of cards called `weapon`.
And there are two cards in this deck with names Sword and Axe, if we generate them running:

```commandline
pysquib game.yaml
```

Everything we create will get saved into a folder named `output` on the same folder as the game.yaml file.
The output should be two images with a name written on top, one says sword, the other says axe.

First let's split the data from the layout, this is a good idea because while the data for cards is bound to change
while prototyping, the layout of the cards tends to be more static, so once defined you can only worry about the data
file. To do that we remove the data from the game.yaml and instead create a `weapon.csv` file with the contents of:

```csv
name
Sword
Axe
```

Pysquib knows to find the `weapon.csv` file because the name of the deck is weapon. It also tries to find other possible
extensions like `weapon.yaml` or `weapon.json`. If you define mutiple of these files all of them will be used.
If you want to use a differently named file you can setup the data key as a string that has either the absolute or the
relative (to the game yaml file) path to the file you want to load.

All of these examples can be found on the examples/minimal folder.

