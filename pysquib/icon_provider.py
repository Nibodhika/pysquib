from __future__ import annotations

import os

from PIL import Image


class IconProvider(object):
    def __init__(self, folders: list = None, missing_icon: str = None):
        self.folders = folders
        self.icons = {}
        self.missing_icon = None

        for folder in self.folders:
            self._load_folder(folder)

        if missing_icon:
            self.missing_icon = self.get(missing_icon)

    def _load_folder(self, folder_path: str):
        files = os.listdir(folder_path)
        for f in files:
            filepath = os.path.join(folder_path, f)
            name = (
                os.path.splitext(f)[0]
                .replace(",", "")
                .replace(".", "")
                .replace(" ", "_")
                .lower()
            )
            if name in self.icons:
                print("Found duplicate of icon with name ", name)
                continue
            self.icons[name] = Image.open(filepath, "r").convert("RGBA")

    def get(self, value):
        out = self.icons.get(value, self.missing_icon)
        if out is None:
            raise KeyError(f'Unknown icon "{value}"')
        return out
