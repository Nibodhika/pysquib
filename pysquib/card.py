from __future__ import annotations

from typing import Dict, Tuple

from PIL import Image, ImageDraw

from pysquib.elements.card_element import CardElement


class Card(object):
    def __init__(
        self,
        width: int,
        height: int,
        elements: Dict[str, CardElement],
        bkg_color: Tuple[int, int, int, int] = (255, 255, 255, 255),
        data: Dict = None,
    ):
        self.width = width
        self.height = height
        self._image = Image.new("RGBA", (self.width, self.height), bkg_color)
        self.drawing = ImageDraw.Draw(self._image)
        self.elements = elements
        if data is not None:
            self.set_data(data)

    def set_data(self, data):
        for attr, elem in self.elements.items():
            value = data.get(attr) or elem.default_value
            elem.value = value
        self.draw()

    def draw(self):
        for _, elem in self.elements.items():
            elem.draw(self._image)

    def show(self):
        self._image.show()

    def save(self, file, file_format="PNG"):
        self._image.save(file, file_format)

    def draw_image(self, xy, image):
        self._image.alpha_composite(image, xy)

    @classmethod
    def create_card(self, card_structure: Dict, dpi: int):
        size = card_structure.get("size", "american")
        card = Card(size, dpi)

        return card
