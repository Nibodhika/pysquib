from __future__ import annotations

from typing import Union, List

from PIL import Image

from pysquib.elements.card_element import CardElement
from pysquib.helpers import scale_preserved_ratio


class IconList(CardElement):
    def __init__(
        self,
        *args,
        icon_width=None,
        icon_height=None,
        vertical=False,
        space_x=0,
        space_y=0,
        elements_per_line=0,
        inverted=False,
        **kwargs,
    ):
        super().__init__(*args, **kwargs)
        self.icon_width = self.game.get_as_pixels(icon_width) or self.width
        self.icon_height = self.game.get_as_pixels(icon_height) or self.height
        self.space_x = self.game.get_as_pixels(space_x)
        self.space_y = self.game.get_as_pixels(space_y)
        self.scale_mode = Image.Resampling.LANCZOS
        self.vertical = vertical
        self.elements_per_line = elements_per_line
        self.inverted = inverted
        self.icons = []
        self.icon_names = []

    @property
    def icon_size(self):
        return self.icon_width, self.icon_height

    def _before_set_value(self, value: Union[str, List[str]]):
        if value is None:
            return
        if not isinstance(value, list):
            value = value.split(",")
        self.icon_names = value
        self.icons = []
        for icon in self.icon_names:
            icon_img = self.game.icon_provider.get(icon)
            self.icons.append(icon_img)

    def _draw(self, image):
        for i, icon in enumerate(self.icons):
            pos_in_line = i % self.elements_per_line if self.elements_per_line else i
            n_line = i // self.elements_per_line if self.elements_per_line else 0
            if self.vertical:
                if self.inverted:
                    y = (self.y + self.height - self.icon_height) - (
                        self.icon_height + self.space_y
                    ) * pos_in_line
                else:
                    y = self.y + (self.icon_height + self.space_y) * pos_in_line

                x = self.x + n_line * (self.icon_width + self.space_x)
            else:
                if self.inverted:
                    x = (self.x + self.width - self.icon_width) - (
                        self.icon_width + self.space_x
                    ) * pos_in_line
                else:
                    x = self.x + (self.icon_width + self.space_x) * pos_in_line
                y = self.y + (self.icon_height + self.space_y) * n_line

            size = scale_preserved_ratio(
                icon.size, self.icon_size, f"icon {self.icon_names[i]}"
            )
            image_to_draw = icon.resize(size, self.scale_mode)
            image.paste(image_to_draw, (x, y), image_to_draw)
