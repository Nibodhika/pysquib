from __future__ import annotations

import os
from PIL import Image

from pysquib.elements.card_element import CardElement
from pysquib.helpers import scale_preserved_ratio


class ImageElement(CardElement):
    def __init__(
        self,
        *args,
        # if set to True will scale as much as posible without distorting image aspect ratio
        preserve_aspect_ratio=True,
        **kwargs,
    ):
        self._image = None
        self.preserve_aspect_ratio = preserve_aspect_ratio
        self.scale_mode = Image.Resampling.LANCZOS
        super().__init__(*args, **kwargs)

    def _before_set_value(self, value):
        if not value:
            return
        if self.folder is not None:
            value = os.path.join(self.folder, value)

        if os.path.exists(value):
            self._image = Image.open(value, "r").convert(
                "RGBA"
            )  # ensure we have a transparency channel
        else:
            print("ERROR: while creating Image, file ", value, " does not exist")

    def _draw(self, image):
        if self._image is None:
            return

        size = self.size
        x = self.x
        y = self.y
        if self.preserve_aspect_ratio:
            size = scale_preserved_ratio(self._image.size, self.size, self.value)
            x = self.x + (self.width - size[0]) // 2
            y = self.y + (self.height - size[1]) // 2

        image_to_draw = self._image.resize(size, self.scale_mode)
        image.paste(image_to_draw, (x, y), image_to_draw)
