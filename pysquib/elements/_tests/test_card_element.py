from pysquib.elements import card_element


def test_card_element_base(game, mocker):

    debug_color = (12, 34, 56)
    mocker.patch.object(card_element, "random_color", return_value=debug_color)

    element = card_element.CardElement(game)  # , width="1mm", height="2mm")
    # default values
    assert element.x == 0
    assert element.y == 0
    assert element.width is None
    assert element.height is None
    assert element.bkg_color is None
    assert element.border_color is None
    assert element.border_width == 0
    assert element.debug_color == debug_color
    assert element.folder is None

    element.x = 1
    element.y = 2
    element.height = 25
    assert element.rect(100, 100) == (1, 2, 99, 25)
    assert element.size == (None, 25)

    element.size = (13, 26)
    assert element.width == 13
    assert element.height == 26
