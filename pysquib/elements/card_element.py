from __future__ import annotations

from typing import Any

from PIL import ImageDraw, Image

from pysquib.helpers import random_color


class CardElement(object):
    def __init__(
        self,
        game: Game,
        x=0,
        y=0,
        width=None,
        height=None,
        bkg_color=None,
        border_color=None,
        border_width=0,
        debug_color=None,
        folder=None,
        default_value=None,
        debug_outline_color=(0, 0, 0),
    ):
        self.game = game
        self.x = self.game.get_as_pixels(x)
        self.y = self.game.get_as_pixels(y)
        self.width = self.game.get_as_pixels(width)
        self.height = self.game.get_as_pixels(height)
        self.bkg_color = bkg_color
        self.border_color = border_color
        self.border_width = border_width
        self.debug_color = debug_color or random_color()
        self.folder = self.game.file(folder)
        self.default_value = default_value
        self._value = None
        self.debug_outline_color = debug_outline_color

    def rect(self, image_width, image_height):
        width = self.width
        height = self.height
        if width is None:
            width = image_width - self.x
        if height is None:
            height = image_height - self.y

        return self.x, self.y, width, height

    @property
    def size(self):
        return self.width, self.height

    @size.setter
    def size(self, sz):
        width, height = sz
        self.width = self.game.get_as_pixels(width)
        self.height = self.game.get_as_pixels(height)

    def draw(self, image):
        if not self.value:
            return

        rect = self.rect(image.width, image.height)

        if self.bkg_color is not None or self.border_color is not None:
            overlay = Image.new("RGBA", image.size, (0, 0, 0, 0))
            drawing = ImageDraw.Draw(overlay, "RGBA")
            if self.bkg_color is not None:
                drawing.rectangle(
                    [(self.x, self.y), (self.x + rect[2], self.y + rect[3])],
                    fill=self.bkg_color,
                )
            if self.border_color is not None:
                x, y, width, height = rect
                half_width = self.border_width / 2 - 1
                drawing.line(
                    [(x, y + half_width), (x + width, y + half_width)],
                    fill=self.border_color,
                    width=self.border_width,
                )
                drawing.line(
                    [(x + width - half_width, y), (x + width - half_width, y + height)],
                    fill=self.border_color,
                    width=self.border_width,
                )
                drawing.line(
                    [
                        (x + width - half_width, y + height - half_width),
                        (x, y + height - half_width),
                    ],
                    fill=self.border_color,
                    width=self.border_width,
                )
                drawing.line(
                    [(x + half_width, y + height - half_width), (x + half_width, y)],
                    fill=self.border_color,
                    width=self.border_width,
                )
            image.paste(overlay, (0, 0), overlay)
        elif self.game.debug:
            overlay = Image.new("RGBA", image.size, (0, 0, 0, 0))
            drawing = ImageDraw.Draw(overlay, "RGBA")
            drawing.rectangle(
                [(self.x, self.y), (self.x + rect[2], self.y + rect[3])],
                fill=self.debug_color,
                outline=self.debug_outline_color,
            )
            image.paste(overlay, (0, 0), overlay)

        self._draw(image)

    def _draw_line(self, drawing, p1, p2, color, width):
        p1_x = self.game.get_as_pixels(p1[0])
        p1_y = self.game.get_as_pixels(p1[1])
        p2_x = self.game.get_as_pixels(p2[0])
        p2_y = self.game.get_as_pixels(p2[1])
        drawing.line([(p1_x, p1_y), (p2_x, p2_y)], fill=color, width=width)

    def _draw(self, image):
        raise NotImplementedError()

    @property
    def value(self):
        return self._value

    @value.setter
    def value(self, value: Any):
        self._before_set_value(value)
        self._value = value

    def _before_set_value(self, value: str):
        """
        Subclasses can implement this if they want to do something extra when setting the value
        :param value:
        :return:
        """
        pass
