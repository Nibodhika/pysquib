from __future__ import annotations

from copy import deepcopy
from typing import Dict, Callable

from pysquib.elements import ImageElement, TextBox, IconList
from pysquib.elements.card_element import CardElement

CARD_ELEMENTS = {
    "image": ImageElement,
    "text": TextBox,
    "icon_list": IconList,
}


def create_element_factory(
    game: Game, element_structure: Dict, card_width: int, card_height: int
) -> Callable[[], CardElement]:
    """
    Returns a function that creates an element with the given parameters
    """
    element_structure = deepcopy(element_structure)
    e_type = element_structure.pop("type")
    element_class = CARD_ELEMENTS[e_type]
    # Use card size as default if not provided
    params = {
        "width": card_width,
        "height": card_height,
        "folder": game.base_folder,
    }
    params.update(element_structure)

    def factory():
        return element_class(game=game, **params)

    return factory
