from __future__ import annotations

import re

from PIL import Image, ImageDraw, ImageFont


from pysquib.elements.card_element import CardElement
from pysquib.helpers import scale_preserved_ratio


class TextBox(CardElement):
    def __init__(
        self,
        *args,
        text_color=(0, 0, 0),
        font=None,
        font_size=10,
        spacing=0,
        margin=None,  # (0,0), # x y. meaning depends on align and valign, center ignores it
        align="left",
        valign="top",
        icon_regex=r"\.:(.*?):\.",  # matches .:icon_name:. format
        **kwargs,
    ):
        super().__init__(*args, **kwargs)
        self.text_color = text_color
        self.font = None
        if font is not None:
            font_size = self.game.get_as_pixels(font_size)
            self.font = ImageFont.truetype(
                self.game.file(font), font_size, layout_engine=ImageFont.Layout.BASIC
            )
        self.spacing = self.game.get_as_pixels(spacing)
        if margin is None:
            self.margin = None
        else:
            self.margin = (
                self.game.get_as_pixels(margin[0]),
                self.game.get_as_pixels(margin[1]),
            )
        self.align = align
        self.valign = valign
        self.icon_regex = icon_regex
        if margin is None:
            self.margin = (self.border_width, self.border_width)

    def _image(self, width, height):
        image = Image.new("RGBA", (width, height))
        drawing = ImageDraw.Draw(image)
        # Check the size of the entire string as a single line to get the height of each line
        one_line_value = self.value.replace("\n", "")
        line_height = drawing.textbbox(
            (0, 0), one_line_value, font=self.font, spacing=0
        )[3]
        whitespace_width = drawing.textbbox((0, 0), " ", font=self.font, spacing=0)[2]

        lines = self.value.split("\n")
        y = 0
        for line in lines:
            str_start = 0
            split_string = []
            icons = []
            max_icon_height = 0
            # attempt to find icons in current line
            for m in re.finditer(self.icon_regex, line):
                before_icon = line[str_start : m.start()]
                split_string.append(before_icon)
                icon_name = m.group(1)
                icon = self.game.icon_provider.get(icon_name)
                icon_size = icon.size
                if icon_size[1] > max_icon_height:
                    max_icon_height = icon_size[1]
                # name is used to have a better debug output if scaling an icon up
                icons.append((icon_name, icon))
                str_start = m.end()

            # String is now split, after each string ends there's an icon
            # if no icon existed, this is just a list with one element
            split_string.append(line[str_start:])

            # Now we calculate how many spaces we need to put in places where there were icons
            icons_list = []
            result_str = split_string[0]
            for index, (icon_name, icon) in enumerate(icons):
                icon_w, icon_h = icon.size

                # Scale height but attempt to preserve width
                icon_w, icon_h = scale_preserved_ratio(
                    icon.size, (icon_w, line_height), f"icon {icon_name}"
                )

                spaces_amount = int(icon_w / whitespace_width) + 1
                # Y position is relative spacing
                icon_pos_y = y + self.spacing + (line_height - icon_h) // 2
                # X position is relative to the width of the last line
                _, _, icon_pos_x, _ = drawing.textbbox(
                    (0, 0), result_str, font=self.font, spacing=0
                )

                next_string = split_string[index + 1]
                result_str += " " * spaces_amount + next_string
                scaled_icon = icon.resize((icon_w, icon_h), Image.Resampling.LANCZOS)
                icons_list.append((icon_pos_x, icon_pos_y, scaled_icon))

            # Now let's see how we must center the text
            actual_size = drawing.textbbox(
                (0, 0), result_str, font=self.font, spacing=self.spacing
            )[2:]
            line_x = line_y = 0
            if actual_size[0] > width or actual_size[1] > height:
                print(f"WARNING: Text \"{self.value} doesn't fit")

            if self.align == "left":
                line_x = self.margin[0]
            elif self.align == "center":
                # center the text
                line_x = int(width / 2 - actual_size[0] / 2)
            elif self.align == "right":
                line_x = int(width - actual_size[0] - self.margin[0])

            if self.valign == "top" and y == 0:
                line_y = self.margin[1]
            if self.valign == "center":
                line_y = int(height / 2 - line_height / 2)
            elif self.valign == "bottom":
                line_y = int(height - line_height - self.margin[1])

            line_y += y
            drawing = ImageDraw.Draw(image, "RGBA")
            drawing.text(
                (line_x, line_y), result_str, fill=self.text_color, font=self.font
            )

            for icon_x, icon_y, icon in icons_list:
                image.paste(icon, (line_x + icon_x, icon_y), icon)

            # before finishig for line add y position
            y = line_y + line_height + self.spacing

        return image

    def _draw(self, image):
        img = self._image(self.width, self.height)
        image.paste(img, (self.x, self.y), img)
        # image.alpha_composite(img, (self.x, self.y))
