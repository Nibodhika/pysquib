import pytest

from pysquib import units


@pytest.mark.parametrize(
    "value,unit,dpi,expected",
    [
        (None, None, None, None),
        # Pixel convertions
        (123, None, None, 123),
        (1.5, None, None, 2),
        (321, None, 600, 321),
        (222, "", None, 222),
        (333, "pixel", None, 333),
        (444, "pixels", None, 444),
        ("555", None, None, 555),
        ("2.2", None, None, 2),
        # Inches
        (1, "inch", None, 300),
        (2, "inches", 600, 1200),
        ("3 inch", None, None, 900),
        # cm
        (1, "cm", None, 118),
        (3, "cm", 600, 709),
        ("4cm", None, None, 472),
        # mm
        (10, "mm", None, 118),
        (20, "mm", 600, 472),
    ],
)
def test_get_as_pixels(value, unit, dpi, expected):
    assert units.get_as_pixels(value, unit, dpi) == expected


def test_wrong_unit():
    with pytest.raises(ValueError) as exc:
        units.get_as_pixels("wrong")
    assert str(exc.value) == 'Impossible to convert "wrong" to a number'


def test_unit_functions(mocker):
    spy = mocker.spy(units, "get_as_pixels")

    unit_functions = {"inch": units.inch, "cm": units.cm, "mm": units.mm}
    for unit, func in unit_functions.items():
        func(123)
        spy.assert_called_once_with(123, unit)
        spy.reset_mock()
