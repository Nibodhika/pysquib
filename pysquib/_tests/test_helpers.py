from unittest.mock import call

import pytest

from pysquib import helpers


@pytest.mark.parametrize(
    "original_size, scaled_size, expected",
    [
        # Square
        ((300, 300), (100, 100), (100, 100)),
        ((300, 300), (300, 150), (150, 150)),
        ((300, 300), (150, 300), (150, 150)),
        # Rectangle X>Y
        ((600, 300), (100, 100), (100, 50)),
        ((600, 300), (500, 150), (300, 150)),
        ((600, 300), (150, 500), (150, 75)),
        # Rectangle Y>X
        ((300, 600), (100, 100), (50, 100)),
        ((300, 600), (500, 150), (75, 150)),
        ((300, 600), (150, 500), (150, 300)),
        # Approximation values get rounded
        ((300, 351), (200, 200), (171, 200)),
    ],
)
def test_scale_preserved_ratio(original_size, scaled_size, expected):
    assert helpers.scale_preserved_ratio(original_size, scaled_size) == expected


def test_scale_up(capfd):
    def check(expected_image_name):
        out, _ = capfd.readouterr()
        assert out == f"WARNING: scaling {expected_image_name} up\n"

    helpers.scale_preserved_ratio((100, 100), (200, 200))
    check("image")
    helpers.scale_preserved_ratio((100, 100), (200, 200), "test")
    check("test")


def test_random_color(mocker):
    randint = mocker.patch.object(helpers, "randint", side_effect=[1, 2, 3])
    assert helpers.random_color() == (1, 2, 3)
    assert randint.call_args_list == [call(0, 255), call(0, 255), call(0, 255)]
