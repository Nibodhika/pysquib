import json
import os
import hashlib
import pytest
import yaml

from pysquib.game import Game
from pysquib.helpers import load_yaml, save_yaml, save_json


def md5(fname):
    """
    Helper function to get the md5 of a file (used to compare expected images)
    """
    hash_md5 = hashlib.md5()
    with open(fname, "rb") as f:
        for chunk in iter(lambda: f.read(4096), b""):
            hash_md5.update(chunk)
    return hash_md5.hexdigest()


def modify_game_data(files, new_value):
    """
    Helper function to modify the contents of game.yaml in place for the test
    to overwrite the value for the data of weapon cards
    """
    gamefile = os.path.join(files, "game.yaml")
    game_data = load_yaml(gamefile)
    game_data["decks"]["weapon"]["data"] = new_value
    save_yaml(gamefile, game_data)


def test_default_game():
    game = Game("test")
    assert game.out_folder == "test/output"
    assert game.dpi == 300
    assert not game.sample_run
    assert not game.debug
    assert game.icon_provider.icons == {}
    assert game.decks == {}


def _check_game(files, game, empty_cards=False):
    assert game.out_folder == os.path.join(files, "out")
    assert game.dpi == 100
    assert not game.sample_run
    assert not game.debug
    assert len(game.icon_provider.icons) == 2
    assert game.icon_provider.get("sword")
    assert game.icon_provider.get("axe")
    assert len(game.decks) == 1
    if empty_cards:
        expected_cards = {}
    else:
        expected_cards = {
            "weapon_1.png": "090631570c08b0ea5ee35cae45b241a6",
            "weapon_2.png": "360b34f9a10ac63a819ecfa1a331ca10",
        }

    assert len(game.decks["weapon"].cards) == len(expected_cards)

    assert not os.path.exists(game.out_folder)
    game.save()
    assert os.path.exists(game.out_folder)

    assert sorted(os.listdir(game.out_folder)) == sorted(list(expected_cards.keys()))

    for card, sumcheck in expected_cards.items():
        card_path = os.path.join(game.out_folder, card)
        assert md5(card_path) == sumcheck


def test_game_from_dict(files):
    """
    Loads the files to memory and builds the game from a native python dictionary
    """
    game_data = load_yaml(os.path.join(files, "game.yaml"))

    game_data["decks"]["weapon"]["data"] = load_yaml(os.path.join(files, "weapon.yaml"))

    game = Game(base_folder=files, **game_data)
    _check_game(files, game)


def test_game_from_yaml(files):
    game = Game.load(os.path.join(files, "game.yaml"))
    _check_game(files, game)


@pytest.mark.parametrize(
    "extension,writer", [("yaml", save_yaml), ("yml", save_yaml), ("json", save_json)]
)
def test_non_default_external_data(files, extension, writer):
    # Move the data file to have a different name (and possibly convert to different format)
    data_file = os.path.join(files, "weapon.yaml")
    new_name_relative = f"different.{extension}"
    new_data_file = os.path.join(files, new_name_relative)
    weapon_data = load_yaml(data_file)
    writer(new_data_file, weapon_data)
    os.remove(data_file)

    # Edit the game.yaml to point to the new data file
    modify_game_data(files, new_name_relative)

    game = Game.load(os.path.join(files, "game.yaml"))
    _check_game(files, game)


def test_wrong_data_file_extention(files):
    modify_game_data(files, "wrong.wrong")
    expected_filename = os.path.join(files, "wrong.wrong")
    with pytest.raises(ValueError) as exc:
        Game.load(os.path.join(files, "game.yaml"))

    assert (
        str(exc.value)
        == f'Unknown data source extention "wrong" when trying to load "{expected_filename}"'
    )


def test_unexisting_data_file(files):
    modify_game_data(files, "wrong.yaml")

    with pytest.raises(FileNotFoundError) as exc:
        Game.load(os.path.join(files, "game.yaml"))


def test_wrong_data_file(files):
    modify_game_data(files, "wrong.yaml")
    save_yaml(os.path.join(files, "wrong.yaml"), {"wrong": "data"})

    with pytest.raises(ValueError) as exc:
        Game.load(os.path.join(files, "game.yaml"))

    assert str(exc.value) == 'Data loaded from "wrong.yaml" is not a list'


def test_game_empty_data(files, capfd):
    modify_game_data(files, [])
    game = Game.load(os.path.join(files, "game.yaml"))
    out, _ = capfd.readouterr()
    assert out == f'WARNING: No data defined for "weapon"\n'
    _check_game(files, game, empty_cards=True)
