from typing import Union
import re


# The dpi used for converting inches and cm to pixels
default_dpi = 300


known_units = {
    "pixel": -1,  # negative numbers ignore dpi
    "pixels": -1,
    "inch": 1,
    "inches": 1,
    "cm": 0.3937,
    "mm": 0.03937,
}


def get_as_pixels(value: Union[int, float], unit: str = None, dpi: int = None) -> int:
    if value is None:
        return None

    if isinstance(value, str):
        # First let's check that this isn't just a number
        match = re.match(r"(-?\d+([.,]\d*)?|[.,]\d+)", value)
        if match is None:
            raise ValueError(f'Impossible to convert "{value}" to a number')
        value_str = match.group(0).replace(",", ".")
        unit = value[match.end() :].strip().lower()
        value = float(value_str)

    if unit == "" or unit is None:
        unit = "pixels"

    if unit in known_units:
        unit_constant = known_units[unit]
        if unit_constant > 0:
            if dpi is None:
                dpi = default_dpi
            unit_constant *= dpi
        else:
            unit_constant *= -1
        return round(unit_constant * value)


def inch(value: Union[float, int]) -> int:
    return get_as_pixels(value, "inch")


def cm(value: Union[float, int]) -> int:
    return get_as_pixels(value, "cm")


def mm(value: Union[float, int]) -> int:
    return get_as_pixels(value, "mm")
