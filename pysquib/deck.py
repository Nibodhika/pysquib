from __future__ import annotations

import os
from typing import Tuple, Union, Dict, List

from pysquib.card import Card
from pysquib.elements.factory import create_element_factory
from pysquib.helpers import load_file


class Deck:
    """
    A deck represents a bunch of cards that have the same structure
    """

    CARD_SIZES = {
        "american": ("57mm", "89mm"),
        "mini_american": ("41mm", "63mm"),
    }

    def __init__(
        self,
        game: Game,
        card_type: str,
        elements: Dict,
        size: Union[str, Tuple[Union[str, int], Union[str, int]]] = "american",
        data: Union[str, List[str]] = None,
        bkg_color: Tuple[int, int, int, int] = (255, 255, 255, 255),
    ):
        original_size = size
        if isinstance(size, str):
            lower_size = size.lower()
            if lower_size not in self.CARD_SIZES:
                raise ValueError(
                    f'Unknown card size "{size}", known sizes are {",".join(self.CARD_SIZES.keys())} or you can specify it as a width, height values'
                )
            size = self.CARD_SIZES[lower_size]
        elif isinstance(size, dict):
            size = size["width"], size["height"]

        if len(size) != 2:
            raise ValueError(f"Invalid size format {original_size}")
        width, height = size

        self.width = game.get_as_pixels(width)
        self.height = game.get_as_pixels(height)
        self.bkg_color = bkg_color

        self.element_factories = {}
        for element_name, element_params in elements.items():
            self.element_factories[element_name] = create_element_factory(
                game, element_params, self.width, self.height
            )

        self.cards = []
        data = self._load_card_data(game, card_type, data)
        self.create_cards(data)

    def create_cards(self, data):
        for card_data in data:
            card = Card(
                self.width,
                self.height,
                bkg_color=self.bkg_color,
                elements={k: v() for k, v in self.element_factories.items()},
                data=card_data,
            )
            self.cards.append(card)

    def _load_card_data(
        self, game: Game, card_type: str, data_source: Union[list, str, dict]
    ):
        ignore_file_not_found = False
        if data_source is None:
            data_source = [
                f"{card_type}.csv",
                f"{card_type}.yaml",
                f"{card_type}.yml",
                f"{card_type}.json",
            ]
            ignore_file_not_found = True

        if not isinstance(data_source, list):
            data_source = [data_source]

        out = []
        for source in data_source:
            if isinstance(source, dict):
                out.append(source)
            elif isinstance(source, str):
                filepath = game.file(source)
                if ignore_file_not_found and not os.path.exists(filepath):
                    continue
                loaded_data = load_file(filepath, accept_csv=True)
                if not isinstance(loaded_data, list):
                    raise ValueError(f'Data loaded from "{source}" is not a list')
                out.extend(loaded_data)
        if not out:
            print(f'WARNING: No data defined for "{card_type}"')

        return out
