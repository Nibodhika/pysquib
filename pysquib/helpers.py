import csv
import json
import os
from typing import Tuple

from random import randint

import yaml


def random_color():
    return (
        randint(0, 255),
        randint(0, 255),
        randint(0, 255),
    )


def scale_preserved_ratio(
    original_size: Tuple[int, int], scaled_size: Tuple[int, int], img_name="image"
) -> Tuple[int, int]:
    """
    Given the original size and the size where the image should fit, scales it keeping the proportions.
    i.e. returns the biggest square with the original_size proportions that fits on the scaled_size
    :param original_size:
    :param scaled_size:
    :param img_name: Optional, can be used to provide a better log of what image had a problem scaling
    :return:
    """
    width = scaled_size[0]
    height = scaled_size[1]

    factor_x = width / original_size[0]
    factor_y = height / original_size[1]
    if factor_x > 1 and factor_y > 1:
        print(f"WARNING: scaling {img_name} up")

    # Using int instead of round to clamp the values
    if factor_x > factor_y:
        # calculate width from height
        width = round((height / original_size[1]) * original_size[0])
    else:
        # calculate height from width
        height = round((width / original_size[0]) * original_size[1])
    return width, height


def load_yaml(filepath):
    with open(filepath, "r") as ifs:
        return yaml.safe_load(ifs)


def save_yaml(filepath, data):
    with open(filepath, "w") as ofs:
        yaml.dump(data, ofs)


def load_json(filepath):
    with open(filepath, "r") as ifs:
        return json.load(ifs)


def save_json(filepath, data):
    with open(filepath, "w") as ofs:
        json.dump(data, ofs)


def load_csv(filepath):
    out = []
    with open(filepath) as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            out.append(row)
    return out


def load_file(filepath, accept_csv=False):
    _, ext = os.path.splitext(filepath)
    ext = ext[1:]
    if ext in ("yml", "yaml"):
        loaded_data = load_yaml(filepath)
    elif ext == "json":
        loaded_data = load_json(filepath)
    elif accept_csv and ext == "csv":
        loaded_data = load_csv(filepath)
    else:
        raise ValueError(
            f'Unknown data source extention "{ext}" when trying to load "{filepath}"'
        )
    return loaded_data
