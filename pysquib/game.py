import os
from typing import List, Dict, Union

from pysquib import units
from pysquib.deck import Deck
from pysquib.helpers import load_file
from pysquib.icon_provider import IconProvider


class Game(object):
    def __init__(
        self,
        base_folder: str,
        out_folder: str = "output",
        dpi: int = 300,
        decks: List[Dict] = {},
        sample_run: bool = False,  # Used to ignore the amount of each card
        icon_provider: Union[list, str] = None,
        missing_icon: str = None,  # Icon to use when trying to reference a non-existing icon, None makes it raise an exception
        debug: bool = False,  # Used to print debug shapes around the elements
        **kwargs,
    ):
        self.base_folder = base_folder
        self.out_folder = os.path.join(base_folder, out_folder)
        self.dpi = dpi
        self.sample_run = sample_run
        self.debug = debug

        icon_folders = []
        if icon_provider is not None:
            if isinstance(icon_provider, str):
                icon_folders = [icon_provider]

        # Make the folders relative to the base_folder
        icon_folders = [self.file(f) for f in icon_folders]

        self.icon_provider = IconProvider(icon_folders, missing_icon)

        self.decks = {}
        for card_type, card_structure in decks.items():
            self.decks[card_type] = Deck(self, card_type=card_type, **card_structure)

    def file(self, folder):
        if folder is None:
            return None
        if not os.path.isabs(folder):
            return os.path.join(self.base_folder, folder)
        return folder

    def get_as_pixels(self, value, unit=None):
        return units.get_as_pixels(value, unit, self.dpi)

    @classmethod
    def load(cls, file):
        base_folder = os.path.abspath(os.path.dirname(file))
        data = load_file(file)

        data.setdefault("base_folder", base_folder)
        return Game(**data)

    def save(self):
        os.makedirs(self.out_folder, exist_ok=True)

        for name, deck in self.decks.items():
            cards = deck.cards
            amount_zeros = len(str(len(cards)))
            for i, card in enumerate(cards):
                card_number = str(i + 1).zfill(amount_zeros)
                out_file = os.path.join(self.out_folder, f"{name}_{card_number}.png")
                card.save(out_file)
