import os
from distutils import dir_util

import pytest

from pysquib.game import Game


@pytest.fixture()
def files(tmpdir, request):
    """
    Copies the contents of the _files dir in the current test path to a temp folder
    then returns a path to it

    This can be used without any parameters, in which case it will use the _files dir, e.g.

    def my_test(files):
        ...

    or it can be used giving it a parameter to specify a different directory, e.g.

    @pytest.mark.parametrize("files", ["_some_other"], indirect=["files"])
    def my_test(files):
        ...

    """
    # request does not have an attribute param if it's not parametrized
    folder = request.param if hasattr(request, "param") else "_files"
    files_dir = os.path.join(request.fspath.dirname, folder)

    if os.path.isdir(files_dir):
        dir_util.copy_tree(files_dir, str(tmpdir))

    return tmpdir


@pytest.fixture(scope="function")
def game(tmpdir):
    game = Game(str(tmpdir))
    yield game
